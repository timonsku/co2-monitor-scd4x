# micropython imports
from machine import I2C, Pin
from . import adafruit_scd4x
import time
import os
import json

# flow3r imports
from st3m.application import Application, ApplicationContext
from ctx import Context
from st3m.input import InputController, InputState
from st3m.utils import save_file_if_changed



class scd4xApp(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        self.input = InputController()
        self.qwiic = I2C(1, freq=400000)
        self.scd4x = adafruit_scd4x.SCD4X(self.qwiic)
        self.settings_path = "/flash/scd4x.json"
        print("SCD4x serial number:", [hex(i) for i in self.scd4x.serial_number])
        self.scd4x.start_periodic_measurement()
        self._co2_buf = []
        self._co2 = 0
        self._temperature = 0
        self._humidity = 0
        self.use_fahrenheit = False

    def on_enter(self, vm):
        super().on_enter(vm)
        self._load_settings()
        self.qwiic = I2C(1, freq=400000)
        self.scd4x = adafruit_scd4x.SCD4X(self.qwiic)
        self.scd4x.start_periodic_measurement()

    def on_exit(self) -> None:
        self.scd4x.stop_periodic_measurement()
        del self.scd4x
        del self.qwiic
        self._co2_buf = []
        self._save_settings()
    
    def _try_load_settings(self):
        if os.path.exists(self.settings_path):
            try:
                with open(self.settings_path) as f:
                    return json.load(f)
            except ValueError:
                print("Failed to parse config file")

    def _load_settings(self):
        settings = self._try_load_settings()
        if settings is None:
            return
        self.use_fahrenheit = settings["use_fahrenheit"]

    def _save_settings(self):
        settings = {}
        settings["use_fahrenheit"] = self.use_fahrenheit
        json_str = json.dumps(settings)
        save_file_if_changed(self.settings_path, json_str)


    def draw(self, ctx: Context) -> None:
        # Get the default font
        ctx.font = ctx.get_font_name(1)
        # Draw a black background
        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()
        ctx.text_align = ctx.MIDDLE
        
        if len(self._co2_buf) < 1:
            ctx.font_size = 25
            ctx.rgb(0.7, 0.7, 0.7)
            ctx.move_to(0, 0).text("Waiting for sensor...")
            return

        ctx.font_size = 40
        yOffset = 45
        yStart = -30
        if(self._co2 > 1000):
            ctx.rgb(0.7, 0.0, 0.0)
        elif(self._co2 > 800):
            ctx.rgb(1, 0.5, 0.0)
        elif(self._co2 > 600):
            ctx.rgb(0.7, 0.7, 0.0)
        else:
            ctx.rgb(0.0, 0.7, 0.0)

        ctx.move_to(0, yStart).text("%d ppm" % self._co2)
        ctx.rgb(0.7, 0.7, 0.7)
        if self.use_fahrenheit:
            ctx.move_to(0, yStart + yOffset * 1).text("%0.1f °F" % (self._temperature * 1.8 + 32))
        else:
            ctx.move_to(0, yStart + yOffset * 1).text("%0.1f °C" % self._temperature)

        ctx.move_to(0, yStart + yOffset * 2).text("%0.1f %%" % self._humidity)

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)
        if self.input.buttons.app.middle.pressed:
            self.use_fahrenheit = not self.use_fahrenheit

        if self.scd4x.data_ready:
            self._co2_buf.append(self.scd4x.CO2)
            if len(self._co2_buf) > 10:
                self._co2_buf.pop(0)
            self._co2 = sum(self._co2_buf) / len(self._co2_buf)
            self._temperature = self.scd4x.temperature
            self._humidity = self.scd4x.relative_humidity

        


# For running with `mpremote run`:
if __name__ == "__main__":
    import st3m.run

    st3m.run.run_app(scd4xApp)
