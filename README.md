# CO2-Monitor-SCD4x

Show CO2 ppm, temperatur and humidity read out from a SCD40/41 CO2 sensor attached to the Qwiic/I2C port of the flow3r

Tested with:
https://www.adafruit.com/product/5187
https://www.adafruit.com/product/5190
